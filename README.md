# Setup notes

## Setup

### Required Software
- bash terminal
- git
- hugo
- scp (or an alternative deployment method)
<!-- - ssh -->

### Windows setup

#### Install and run git-bash

1. Download and install Git for Windows
   
   This can be downloaded from https://git-scm.com/download/win

   Make sure to install "git-bash" during the setup process"

2. Open git-bash

   search for git-bash from the start menu and open it

3. Add a name and email address (for identifying your commits)

   git config --global user.email "you@example.com"
   git config --global user.name "Your Name"

<!-- #### Set up SSH key

1. Set up SSH key for connecting to the server

   ```
   ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
   eval "$(ssh-agent -s)"
   ssh-add ~/.ssh/id_rsa
   ```

2. Copy your public key and add it to the Git repository account

   To view the public key, type the following

   ```
   cat ~/.ssh/id_rsa.pub
   ```

   Then go the following page to add this output

   https://git.disroot.org/user/settings/keys -->

#### Copy and Install the Project

1. Clone the repository

   <!-- ```
   mkdir project-folder
   cd project-folder
   git clone --recursive git@git.disroot.org:ace/hugo-demo.git
   cd hugo-demo
   ``` -->

   ```
   mkdir project-folder
   cd project-folder
   git clone --recursive https://git.disroot.org/ace/hugo-demo.git
   cd hugo-demo
   ```

2. Install Hugo

   ```
   ./install/hugo/git-bash.sh
   ```

3. Run setup script

   ```
   ./setup.sh
   ```

## Workflow

Check remote servers for changes

    ./check.sh

Pull changes

    ./pull.sh

Start local test server

    ./test.sh

Push changes to remote git repositories

    ./push.sh

Build changes locally

    ./build.sh

Build and deploy changes

    ./deploy.sh