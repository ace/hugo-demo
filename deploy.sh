#!/bin/sh

# server info

# USER=ace
# HOST=j25.webarch.net
# REMOTE_DIR=sites/ace   # the directory where your web site files should go
# PORT=22

USER=test@hugotest.autonomous.org.uk
HOST=ray.shared.1984.is
REMOTE_DIR=htdocs   # the directory where your web site files should go
PORT=2222

# local build directory path
DIR="public"

# clear build directory
rm -r "$DIR"

# re-build website
hugo

# check that build directory exists
if [ -d "$DIR" ]
then
  # check that build directory is not empty
	if [ "$(ls -A $DIR)" ]; then
    # push to public web server location

    # via scp - this will not delete old files
    echo "scp -r -P ${PORT} -o User=${USER} $DIR/* ${HOST}:${REMOTE_DIR}"
    scp -r -P ${PORT} -o User=${USER} -o HostKeyAlgorithms=+ssh-rsa $DIR/* ${HOST}:${REMOTE_DIR} && echo "Deployment Successful" && git restore --staged . && git add public && git commit -m "deployed changes to $HOST" && echo "Deployed changes added to git" || echo "No changes made to the git repository"

    # via rsync - this command will delete everything on the server that's not in the local build folder
    # rsync -avz --delete "$DIR"/ ${USER}@${HOST}:~/${REMOTE_DIR} && git restore --staged . && git add public && git commit -m "deployed changes to server" && echo "Changes deployed to $HOST" || echo "No changes deployed to $HOST"
	else
    # if build fails restore the old build directory
    git restore "$DIR"
	fi
else
	echo "Directory $DIR not found."
  # if build fails restore the old build directory
  git restore "$DIR"
fi

exit 0
