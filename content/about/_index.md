---
title: "About"
description: "About ACE"
draft: false
tags: []
categories: []
---


autonomy: independence, independent; the right of self government; an independent state or community; freedom to act as one pleases; freedom of will.

ACE is run collectively by a friendly group who are drawn from the variety of groups and individuals who use ACE for their various activities.

We share a vision of a world without bosses and so try to work together non-hierarchically in a spirit of respect and friendship. By sharing experiences, skills and knowledge, we hope to help each other improve our homes, communities, workplaces and lives. We try to contribute to the struggles going on around the world that aim to transform our society for the better.