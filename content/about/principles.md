---
title: "ACE Principles: What We Believe & Do"
description: "ACE Principles"
draft: false
tags: []
categories: []
---

The Autonomous Centre of Edinburgh (ACE) is a self-managed, independent social centre run entirely by the people and the groups involved.  It works on a non-hierarchical basis: no bosses, no managers and all decisions are made collectively centred on its monthly assembly.

Through ACE we are trying to create a culture of resistance to the advances of neoliberal capitalism, creating structures that allow us to reorganise aspects of our lives and needs. We do not seek to represent people, rather we stand by those who, themselves, choose to struggle.

We promote solidarity as our central principle in the fight against all forms of oppression. Overthrowing the oppression of women and restrictive gender roles, ending racism and the plunder of the global south, living in ecological harmony with the earth and other species, and the revolutionary reclaiming of resources to meet human need, are all struggles ACE supports.

We believe that the many struggles for justice need to draw together to create one fight against the whole system of exploitation. Fundamental, revolutionary change is needed.  Our vision is a world where all co-operate as equals.

**LET’S GET ORGANISED!**

We do not look to politicians, bureaucrats or bosses for solutions. We realise that only by working together against exploitation, domination and hierarchy can we build a free stateless world where people co-operate as equals and hold the planet in common. To make effective changes we believe that people need to take matters into their own hands and get organised.

ACE regards calls for political elites to behave more fairly, appealing to an illusory ideal of ‘democracy’, to be fruitless in the long term. Politicians pursue the ‘National Interest’ which we take to mean the interests of the ruling class as we do not accept that exploiters and exploited share a common interest. To pursue change through parliamentary and electoral channels is to see our struggles dissipated, defused and co-opted. While states like to describe themselves as ‘democratic’ they are a world away from a truly democratic society. Their alleged ‘democracy’ has been adjusted over centuries to appear democratic while leaving the same fundamentally anti-democratic forces in power.

**LINKING UP**

But shouldn’t we be ‘realistic’ and work within established power structures? ACE considers that when the term ‘realistic’ is used in this way it is just a code for ‘doing what the rich want’. We do not accept that our struggles must forever obey this false logic. This is why ACE believes that no political leadership can be trusted, that they are cynical and manipulative by nature. We instead propose that only solidarity based on recognition of shared interests can challenge the bankrupt, greed fuelled economic system. So we actively seek to encourage links between different sections of the working class, e.g. employed and unemployed, recent immigrants and long-term residents – we reject nationalist and other divisions.

We believe that with enough people direct action can make the rule of bosses and their political protectors unworkable. We aim to establish a truly democratic counter-power, pressurising authorities in the here and now and giving people a means to defend themselves in their daily lives, whilst simultaneously challenging the whole capitalist system, demanding and working towards a world without classes, borders and oppressive elites. Such a world would do away with production for profit – instead people would co-operate as equals to directly satisfy human needs. Along with ending class oppression and inequality it is vital that other fundamental and intertwined forms of oppression such as racism and patriarchy – the oppression of women by men – are consciously combated and swept away.

**GET INVOLVED!** If you agree with this statement and are happy to work together collectively and with respect for others, in harmony with ACE Safer Spaces Practices, then we invite you to get involved in ACE – please contact us for more details.

 

**NOTE RE ACE STATEMENT : “AUTONOMOUS CENTRE OF EDINBURGH What we believe and do”**

Agreement to this statement, adherence to ACE Safer Spaces Practices, and regular involvement in ACE work, is the basis for membership of the ACE Working Group collective and also for the participation of groups which are central to the working of ACE (ECAP, SRL, Infoshop). Of course this does not prevent other groups booking meeting space in ACE.

Membership of the working group includes the right to participate in the working group email list and the right to vote at meetings, on the rare occasion there is a vote.