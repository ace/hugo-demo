#!/bin/sh

# local build directory path
DIR="public"

# clear build directory
rm -r "$DIR"

# re-build website
hugo

# check that build directory exists
if [ -d "$DIR" ]
then
  # check that build directory is not empty
	if [ ! "$(ls -A $DIR)" ]; then
    # if build fails restore the old build directory
    git restore "$DIR"
	fi
else
	echo "Directory $DIR not found."
  # if build fails restore the old build directory
  git restore "$DIR"
fi

exit 0
