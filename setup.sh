#git remote set-url j25 ace@j25.webarch.net:/home/ace/private/git/ace.j25.org 2> /dev/null || git remote add j25 ace@j25.webarch.net:/home/ace/private/git/ace.j25.org
#git remote set-url disroot git@git.disroot.org:ace/hugo-demo.git 2> /dev/null || git remote add disroot git@git.disroot.org:ace/hugo-demo.git
git remote set-url disroot https://git.disroot.org/ace/hugo-demo.git 2> /dev/null || git remote add disroot https://git.disroot.org/ace/hugo-demo.git
git remote -v update
git branch -u disroot/main

# TODO:
# - add install scripts here once checks put in place
# - add code which will add the other scripts to $PATH so they are less clunky to call