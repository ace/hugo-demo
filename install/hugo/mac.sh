which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    brew update
fi

LIB=hugo
# install if we haven't installed any version
brew ls --versions $LIB || brew install $LIB
# install if we haven't installed latest version
brew outdated $LIB || brew install $LIB

