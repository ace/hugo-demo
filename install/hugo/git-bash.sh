curl -L -O https://github.com/gohugoio/hugo/releases/download/v0.111.3/hugo_extended_0.111.3_windows-amd64.zip
unzip hugo_extended_0.111.3_windows-amd64.zip -d hugo


mkdir -p "$HOME/bin/hugo"
mv hugo/hugo.exe "$HOME/bin/hugo"

touch "$HOME/.bashrc"
for f in "$HOME/.bashrc" "$HOME/.bash_profile"
do
	echo "Processing $f"
	if [ -f "$f" ] ; then
		if grep -q "export PATH=\$PATH:$HOME/bin/hugo" "$f" ; then
			echo "Hugo initialization already in file " $f
			break
		else
			echo "Appending Hugo initialization to " $f
			echo "[ -f $HOME/bin/hugo/hugo.exe ] && export PATH=\$PATH:$HOME/bin/hugo" >>"$f"
      exec bash
			break
		fi
	fi
done

rm -r hugo
rm hugo_extended_0.111.3_windows-amd64.zip